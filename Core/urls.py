from Core.models import Contact
from django.urls import path
from . import views

app_name = 'Core'
urlpatterns = [
    
    path('',views.index , name="index"),
    path('api/contact' , views.ContactData , name = "ContactForm"),
    path('api/work' , views.worksData , name = "worksData"),
    path('api/work-details/<int:pk>/' , views.workDetails ,name = "workDetaile" )
   
]