from django.db import models
from django.db.models.fields import CharField, DateField

# Create your models here.

class Home(models.Model):
   name =  models.CharField(  null = True , blank=True , max_length=50 , verbose_name = "الاسم الخاص بي")
   slogan = models.CharField( null = True , blank=True , max_length=50, verbose_name = "الشعار")
   background = models.ImageField( null = True , blank=True , verbose_name = "الخلفية الاساسية")

   facbook = models.CharField(  null = True , blank=True ,max_length=200 , verbose_name = "facbook" )
   linked_in = models.CharField( null = True , blank=True , max_length=200 , verbose_name = "linked_in" )
   git_hub = models.CharField( null = True , blank=True , max_length=200 , verbose_name = "git_hub" )
   instagram = models.CharField( null = True , blank=True , max_length=200 , verbose_name = "instagram" )
   behance = models.CharField( null = True , blank=True , max_length=200 , verbose_name = "behance" )

   def __str__(self) :
      return self.name
   

class About(models.Model):
  job_title = models.CharField(max_length=50, verbose_name = "عنوان الوظيفة" , null = True ,blank=True)
  job_discraption = models.TextField( verbose_name = "تفاصيل العمل" , null = True ,blank=True)
  website = models.CharField(max_length=50, verbose_name = "الموقع" , null = True ,blank=True)
  city = models.CharField(max_length=15, verbose_name = "المدينة" , null = True ,blank=True)
  age = models.IntegerField(verbose_name="السن" , null = True ,blank=True)
  phone = models.CharField(max_length=20,verbose_name="رقم الموبيل" , null = True ,blank=True)
  degree = models.CharField(max_length=20, verbose_name = "الدرجة العلمية" , null = True ,blank=True)
  birthday = models.DateField(auto_now=False, auto_now_add=False , verbose_name="تاريخ الميلاد" , null = True ,blank=True)
  my_discraption = models.TextField( verbose_name = "تفاصيل شخصية" , null = True ,blank=True)
  freelancer = models.BooleanField(default=True , verbose_name="متاح للعمل الحر" , null = True ,blank=True)
  image = models.ImageField(verbose_name="الصورة" , null = True ,blank=True)

  def __str__(self):
      return self.job_title
       

class Counter(models.Model):
   happy_client =models.IntegerField(verbose_name = "عدد العملاء")
   projects =models.IntegerField(verbose_name = "عدد المشاريع")
   courses =models.IntegerField(verbose_name = "عدد الكورسات")
   certifications =models.IntegerField(verbose_name = "عدد الشهادات")

class Skills(models.Model):
   title = models.CharField(max_length=50 , verbose_name="المهارة")
   avarage = models.IntegerField(verbose_name="النسبة")

   def __str__(self):
       return self.title


class INTEREST(models.Model):
   icons = models.CharField( max_length=200 , verbose_name = "icons")
   title = models.CharField( max_length=50 , verbose_name = "العنوان")
   

   def __str__(self):
      return self.title       


class Contact(models.Model):
   name = models.CharField(max_length=50)
   email = models.EmailField()
   subject = models.CharField(max_length=100)
   message = models.TextField()
   deleted  = models.BooleanField(default=False)

   def __str__(self):
      return self.name


class Sumary(models.Model):
   title = models.CharField(max_length=100 , verbose_name = "العنوان")
   sumary = models.TextField(verbose_name = "الملخص")
   lists = models.CharField(max_length=50 , null=True , )

   def __str__(self):
          return self.title

class Eduction(models.Model):
   title = models.CharField(max_length=50 , verbose_name="العنوان")
   from_date = models.DateField()
   to_date = models.DateField()
   heading = models.CharField( max_length=100)
   discraption = models.TextField()

   def __str__(self):
      return self.title

class Experince(models.Model):
   title = models.CharField(max_length=50)
   company = models.CharField(max_length=50)
   from_date = models.CharField(max_length=50)
   to_date =  models.CharField(max_length=50)
   work_discraption = models.TextField()

   def __str__(self):
      return self.title      


class Services(models.Model):
   title = models.CharField(max_length=50)
   icons = models.CharField(max_length=50)
   discraption = models.TextField()

   def __str__(self):
      return self.title      

class Category(models.Model):
   name = models.CharField( max_length=50)   

   def __str__(self):
      return self.name   

class Works(models.Model):
   slide1 = models.ImageField(upload_to='works/' , null=True , blank=True)
   slide2 = models.ImageField(upload_to='works/', null = True , blank = True)
   slide3 = models.ImageField(upload_to='works/', null = True , blank = True)
   slide4 = models.ImageField(upload_to='works/', null = True , blank = True)
   slide5 = models.ImageField(upload_to='works/', null = True , blank = True)
   model_image = models.ImageField(upload_to='works/', )
   name = models.CharField( max_length=50)
   category = models.ForeignKey(Category,on_delete=models.CASCADE)
   client = models.CharField( max_length=50)
   project_date =models.DateField( auto_now=False, auto_now_add=False)
   project_url = models.CharField( max_length=100)
   discraption = models.TextField()
   git_hub = models.CharField( null=True ,max_length=50)

   def __str__(self):
      return self.name
   
class Testimonial(models.Model):
   name = models.CharField( max_length=50)
   job = models.CharField( max_length=50)
   image = models.ImageField(upload_to='client/', height_field=None, width_field=None, max_length=None)
   message = models.TextField()

   def __str__(self):
      return self.name
   