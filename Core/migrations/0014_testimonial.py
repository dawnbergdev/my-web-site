# Generated by Django 3.2.7 on 2021-09-15 19:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0013_works'),
    ]

    operations = [
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('job', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='client/')),
                ('message', models.TextField()),
            ],
        ),
    ]
