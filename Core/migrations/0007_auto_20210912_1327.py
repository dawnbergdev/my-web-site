# Generated by Django 3.2.7 on 2021-09-12 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0006_auto_20210912_1314'),
    ]

    operations = [
        migrations.AddField(
            model_name='interest',
            name='color',
            field=models.CharField(default=1, max_length=50, verbose_name='اللون'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='interest',
            name='title',
            field=models.CharField(max_length=50, verbose_name='العنوان'),
        ),
    ]
