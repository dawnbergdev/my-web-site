# Generated by Django 3.2.7 on 2021-09-15 18:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0010_contact_deleted'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sumary',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='العنوان')),
                ('sumary', models.TextField(verbose_name='الملخص')),
                ('lists', models.CharField(max_length=50, null=True)),
            ],
        ),
    ]
