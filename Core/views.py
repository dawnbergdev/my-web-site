from django.http import response
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from .models import * 
from .forms import *
from django.contrib import messages

# Create your views here.

def index (request):
    homePage = Home.objects.all().last()
    about= About.objects.all().last()
    counter = Counter.objects.all().last()
    skills = Skills.objects.all()
    interest = INTEREST.objects.all()
    sumary = Sumary.objects.all().last()
    education = Eduction.objects.all().order_by("-id")
    experince = Experince.objects.all().order_by("-id")
    services = Services.objects.all()
    allworks = Works.objects.all()
    
    print(sumary)

    form = ContactForm()


    context = {
        'homepage': homePage,
        'about' : about,
        'counter' : counter,
        'skills' : skills,
        'interest' : interest,
        'sam' : sumary , 
        'edu' : education,
        'ex' :experince,
        'ser' : services,
        'form' : form,
        'allworks' : allworks
        
    }
    return render(request, 'Core/index.html' , context )
    

def ContactData(request):
    if request.is_ajax():
        name =request.POST.get('name' , None) # getting data from name input 
        email =request.POST.get('email' , None) # getting data from email
        subject =request.POST.get('subject' , None) # getting data from subject input 
        message =request.POST.get('message' , None) # getting data from message input 
        if name and email and subject and message:  # checking if name and email and subject and message have value
            data = Contact(
                name = name,
                email = email,
                subject = subject,
                message = message
            )
            data.save()
            response = {
                'msg' : 'Send Successfully'
            }
            return JsonResponse(response)


def worksData(request):
    if request.is_ajax():
        allworks = Works.objects.all()
        response = {
            'allworks' :allworks
        }
        return render(request , 'Core/allworks.html' ,response)

def workDetails(request , pk):
    work = Works.objects.get(id=pk)
    return render(request , 'Core/work.html' ,{'work':work})
