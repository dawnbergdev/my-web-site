from django import forms
from .models import *


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {

            'name' : forms.TextInput(attrs={'class' : 'form-control' ,'id':'name' ,'placeholder':'Your Name'}) , 

            'email' : forms.EmailInput(attrs={'class' : 'form-control' , 'name' : 'email' , 'id' : 'email' , 'placholder' : 'Yor Email Here','data-rule' : 'email' , 'data-msg' : 'please enter a valid email'}) , 

            'subject' : forms.TextInput(attrs={'class' : 'form-control' , 'name' : 'subject' , 'id' : 'subject' , 'placholder' : 'subject','data-rule' : 'minlen:4' , 'data-msg' : 'Please enter at least 8 chars of subject'}),

            'message' : forms.Textarea(attrs={'class' : 'form-control' , 'name' : 'message' ,'rows' : '5', 'id' : 'message' , 'placholder' : 'Message','data-rule' : 'required' , 'data-msg' : 'Please write something for us'}),

            'deleted' : forms.HiddenInput()

        }



