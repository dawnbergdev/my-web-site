
from django.contrib import admin
from . models import *

# Register your models here.
admin.site.register(Home)
admin.site.register(About)
admin.site.register(Counter)
admin.site.register(Skills)
admin.site.register(INTEREST)
admin.site.register(Contact)
admin.site.register(Sumary)
admin.site.register(Services)
admin.site.register(Category)
admin.site.register(Works)
admin.site.register(Testimonial)
admin.site.register(Experince)
admin.site.register(Eduction)
